package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbByetConn {
	public static Connection getConnection() {
		Connection con = null;
		String url = "jdbc:mysql://sql302.byethost14.com/b14_35929318_bts";
		
		try {
		
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			//2. Establishing the Connection
			con = DriverManager.getConnection(url, "b14_35929318", "pavIAS2024");
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
}
