package day3;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Rsmd {

	public static void main(String[] args) {
		
Connection connection = DbConnection.getConnection();
		
		PreparedStatement preparedStatement = null;
		
		ResultSet resultSet = null;
		ResultSetMetaData rsmd1 = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter Employee empId:");
		
		int empId = scanner.nextInt();
		
		String selectQuery = "select * from employee2 where empId = ?";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
						
			preparedStatement.setInt(1, empId);
			
			resultSet = preparedStatement.executeQuery();
			rsmd1 = resultSet.getMetaData();
			
			if (resultSet != null) {

				while(resultSet.next()) {

					System.out.println("Total columns: "+rsmd1.getColumnCount());  
					System.out.println("Column Name of 1st column: "+rsmd1.getColumnName(1));  
					System.out.println("Column Type Name of 1st column: "+rsmd1.getColumnTypeName(1));  
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

			} 
			else {
				System.out.println("No Record(s) Found!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		



	}

}