package day1;

import java.sql.Connection;


import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Update {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		
		String empname = "pavani";
		double salary = 2000.89;
		
		String upadteQuery = "update employee2 set salary = " + salary + " where empname = ' " + empname + " '";
		
		try {
			st = con.createStatement();
			// dml = executeupdate - insert, update, delete
			// dql = executequery
			// 
			int res = st.executeUpdate(upadteQuery);
			if(res > 0) {
				System.out.print(res);
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
