package day1;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo1 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		int empId = 107;
		String empname = "Pavani";
		double salary = 1888.89;
		String gender = "female";
		String email = "pav@gmail.com";
		String password = "123";
		String insertQuery = "insert into employee values(" + empId + ", ' " + empname + " ', " + salary + ", ' " + gender + " ',' " + email + " ',' " + password + " ')";
		
		try {
			st = con.createStatement();
			// dml = executeupdate - insert, update, delete
			// dql = executequery
			//
			int res = st.executeUpdate(insertQuery);
			if(res > 0) {
				System.out.print(res);
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}





