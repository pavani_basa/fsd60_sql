package day2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.Scanner;

import com.db.DbConnection;

public class Delete {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		PreparedStatement st = null;
		Scanner sc = new Scanner(System.in);
		int empid = sc.nextInt();
		
		String deleteQuery = "delete from employee2 where empid=?";
		
		try {
			st = con.prepareStatement(deleteQuery);
			
			st.setInt(1, empid);
			int res = st.executeUpdate();
			if(res > 0) {
				System.out.print("deleted");
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
