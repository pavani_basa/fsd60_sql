package day2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class PrepStatement {
	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		PreparedStatement st = null;
		
		Scanner sc = new Scanner(System.in);
		int empId = sc.nextInt();
		String empname = sc.next();
		double salary = sc.nextDouble();
		String gender = sc.next();
		String email = sc.next();
		String password = sc.next();
		
		String insertQuery = "insert into employee2 values (?, ?, ?, ?, ?, ?)";
		try {
			st = con.prepareStatement(insertQuery);
			st.setInt(1, empId);
			st.setString(2, empname);
			st.setDouble(3, salary);
			st.setString(4, gender);
			st.setString(5, email);
			st.setString(6, password);
			
			int res = st.executeUpdate();
			
			if(res > 0) {
				System.out.println("Record inserted");
			}
			else {
				System.out.println("Record not inserted");
			}
			
			st.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	    try {
	    	if(con != null){
			con.close();
	    	}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
	}

}
