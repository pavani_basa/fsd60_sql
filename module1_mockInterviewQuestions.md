## Module1 Mock Interview Questions:

0. Prepare your introduction
1. React Lifecycle
2. Do we have transaction safety in mongodb
3. MongoDB crud operations
4. Example of an arrow function  
5. Basic example of react js component
6. Basic examples of useState,useEffect
7. What is mongodb
8. Diff b/w nodejs and expressjs.
9. Es6 concepts in javascript
10. Write a function in js
11. Write code to run express project.
12. Write a function that returns sum of 2 numbers in js
13. Reverse an array in js without inbuilt methods
14. JS function currying
15. Diff b/w for each and map
16. How do u invoke express server
17. Functional and class components
18. What are hofs and examples
19. Write map function
20. How to update message in functional component
21. Const keyword in js
22. Diff b/w let and const
23. What is react js
24. Write an example of a function that returns another function
25. Write basic react component structure
26. write a simple js function,take 2 numbers and give product of the numbers as output,
27. async functions in js, promises, basic component in reactjs
28. state and props,hooks in react js
29. diff b/w rdbms and non-rdbms
30. importing ../css is relative or absolute import
31. explain app.js routing app.get('/')
32. explain css code in your project
33. css editing in the inspect window.

34. diff b/w props and state in react js
35. Is sql rdbms or non rdbms

36. write react js component to print hello world output
37. life cycle components  in reactjs
38. what happens when a function is called many number of times
39. what is Single page applications
40. Difference b/w post and put requests.

41. asynchronous programming in js
42. box model in css, diff b/w padding and margin