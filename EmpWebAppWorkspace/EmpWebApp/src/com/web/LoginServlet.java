package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDao;
import com.dto.Employee;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		// store emailid under httpsession object
		
		HttpSession session = request.getSession(true);
		session.setAttribute("email", email);
		
		
		
		if(email.equalsIgnoreCase("pav@gmail.com") && password.equals("1234")) {
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
			rd.forward(request, response);
			
		}
		else {
			EmployeeDao empDao = new EmployeeDao();
			Employee emp = empDao.empLogin(email, password);
			if (emp != null) {
				
				//Storing Employee Object Under Session
				session.setAttribute("employee", emp);
							
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("EmpHomePage");
				requestDispatcher.forward(request, response);

			} else {
				out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
				requestDispatcher.include(request, response);
			}
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
