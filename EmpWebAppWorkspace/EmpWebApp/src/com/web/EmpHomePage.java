package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EmpHomePage
 */
@WebServlet("/EmpHomePage")
public class EmpHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		HttpSession session = request.getSession(false);
		String email = (String) session.getAttribute("email");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow'>");				
			
		out.print("<form align='right'>");
		out.print("<a href='EmpHomePage'>Home</a> &nbsp;");

		out.print("<a href='Logout'>Logout</a>");
		out.print("</form>");
			
		out.print("<h3>Welcome: " + email + "!</h3>");
			
		out.print("<center>");
		out.print("<h1 style='color:green'>Welcome to EmpHomePage</h1>");	
			
		//Providing the Profile Link
		out.print("<h3><a href='Profile'>Profile</a></h3>");
		out.print("<center></body></html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
