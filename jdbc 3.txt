JDBC
****

JDBC Day-03
***********



Delete Employee Record
----------------------
package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.db.DbConnection;

//Delete Employee Record Based on EmpId
public class Demo01 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		String deleteQuery = "Delete from Employee where empId = ?";
		
		System.out.print("Enter Employee ID: ");
		int empId = new java.util.Scanner(System.in).nextInt();
		
		try {
			preparedStatement = connection.prepareStatement(deleteQuery);
			preparedStatement.setInt(1, empId);
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Employee with empId: " + empId + " Deleted Successfully!!!");
			} else {
				System.out.println("Failed to Delete the Employee Record!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}




---------------------------------------------------------------------



Select All Employees
--------------------


package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Fetch All Employees
public class Demo02 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from Employee";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}





---------------------------------------------------------------------



Select Employee Based on EmpId
------------------------------
package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Fetch Employee Based on EmpId
public class Demo03 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from Employee where empId = ?";
		
		System.out.print("Enter Employee Id: ");
		int empId = new java.util.Scanner(System.in).nextInt();
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setInt(1, empId);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble("salary"));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
			} else {
				System.out.println("Unable to Fetch Employee Record");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}







---------------------------------------------------------------------



ResultSetMetaData (For Displaying Column Names)
-----------------------------------------------


package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.db.DbConnection;

//Demonstrating ResultSetMetaData for Showcasing Column Names
public class Demo04 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultSetMetaData = null;
		
		String selectQuery = "Select * from Employee where empId = ?";
		
		System.out.print("Enter Employee Id: ");
		int empId = new java.util.Scanner(System.in).nextInt();
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setInt(1, empId);
			resultSet = preparedStatement.executeQuery();
			resultSetMetaData = resultSet.getMetaData();
			
			if (resultSet.next()) {
				System.out.print("********************************************************************\n");
				
				System.out.print("* " + resultSetMetaData.getColumnName(1) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(2) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(3) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(4) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(5) + "          ");
				System.out.print("* " + resultSetMetaData.getColumnName(6) + " * \n");
				
				System.out.print("*******************************************************************\n");
				
				
				System.out.print("* " + resultSet.getInt(1) + "   ");
				System.out.print("* " + resultSet.getString(2) + "  ");
				System.out.print("* " + resultSet.getDouble(3) + "  ");
				System.out.print("* " + resultSet.getString(4) + " ");
				System.out.print("* " + resultSet.getString(5) + " ");
				System.out.print("* " + resultSet.getString(6) + "      * ");
				System.out.print("\n");
				
				System.out.print("*******************************************************************\n");
				
			} else {
				System.out.println("Unable to Fetch Employee Record");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}





---------------------------------------------------------------------


package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.db.DbConnection;

//Demonstrating ResultSetMetaData for Showcasing Column Names
public class Demo05 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultSetMetaData = null;

		String selectQuery = "Select * from Employee";

		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
			resultSetMetaData = resultSet.getMetaData();

			if (resultSet != null) {

				System.out.print("********************************************************************\n");

				System.out.print("* " + resultSetMetaData.getColumnName(1) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(2) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(3) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(4) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(5) + "          ");
				System.out.print("* " + resultSetMetaData.getColumnName(6) + " * \n");

				System.out.print("*******************************************************************\n");


				while (resultSet.next()) {
					System.out.print("* " + resultSet.getInt(1)    + "   ");
					System.out.print("* " + resultSet.getString(2) + "  ");
					System.out.print("* " + resultSet.getDouble(3) + "  ");
					System.out.print("* " + resultSet.getString(4) + " ");
					System.out.print("* " + resultSet.getString(5) + " ");
					System.out.print("* " + resultSet.getString(6) + "      * ");
					System.out.print("\n");

				}
				System.out.print("*******************************************************************\n");

			} else {
				System.out.println("Unable to Fetch Employee Record");
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}





---------------------------------------------------------------------




Program Line a Normal Application to insert an Employee Record




package day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class Demo06 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String selectQuery = "Select * from Employee";

		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("\n");
		
		//-------------------------------------------------------

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee Details");

		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String emailId = scanner.next();
		String password = scanner.next();
		System.out.println();

		String insertQuery = "Insert into employee values (?, ?, ?, ?, ?, ?)";

		try {
			connection = DbConnection.getConnection();
			preparedStatement = connection.prepareStatement(insertQuery);

			preparedStatement.setInt(1, empId);
			preparedStatement.setString(2, empName);
			preparedStatement.setDouble(3, salary);
			preparedStatement.setString(4, gender);
			preparedStatement.setString(5, emailId);
			preparedStatement.setString(6, password);

			int result = preparedStatement.executeUpdate();

			if (result > 0) {
				System.out.println("Record Inserted into the table");
			} else {
				System.out.println("Record Insertion Failed!!!");
			}

			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n");
		
		//---------------------------------------------------------------

		try {
			connection = DbConnection.getConnection();
			preparedStatement = connection.prepareStatement("Select * from employee");
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}



---------------------------------------------------------------------

package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Login Using EmailId and Password
public class Demo07 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from Employee where emailId = ? and password = ?";
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Email-Id: ");
		String emailId = scanner.next();
		System.out.print("Enter Password: ");
		String password = scanner.next();
		System.out.println();
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setString(1, emailId);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				System.out.println("LoginStatus: Login Success \n");
				
				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble("salary"));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
			} else {
				System.out.println("LoginStatus: Login Failed, Invalid Credentials");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}


---------------------------------------------------------------------





